Sistema de computaci�n 2019

Robot hex�podo accionado por Raspberry

El proyecto consiste en el desarrollo de un robot hex�podo accionado por Raspberry, el usuario puede controlar sus movimientos, tambi�n puede funcionar en forma aut�noma. Sus tres servomotores le permiten caminar, un cuarto servomotor es utilizado  para rotar un sensor ultras�nico que detecta obst�culos. En el modo autom�tico el hex�podo camina hacia adelante hasta que detecta un obst�culo y retroceda un par de pasos y luego se mueva hacia la derecha.

El robot hex�podos es controlado por una Raspberry Pi 3 B+ a trav�s de Python usando la biblioteca Pigpio.�B�sicamente, los movimientos del robot siguen la marcha del tr�pode, una forma de caminar seguida por la mayor�a de los insectos.
Se agreg� un m�dulo en c, la funcionalidad que proporciona es, luego de detectar un obst�culo, activa leds que parpadean antes de retroceder, avisando que un obst�culo est� pr�ximo.


El link del proyecto sobre el que nos basamos:

https://hackaday.io/project/25915-kembot


El hex�podo caminando


https://drive.google.com/file/d/1227wwya6pLrE_FUOzCllvkGj0MrUXUKT/view?usp=sharing


Sobre la organizaci�n del directorio 


- Siguiendo el esquema de directorio \sistemas_de_computacion\Documentos


El archivo INFORME.docx contiene una descripci�n del funcionamiento de movimiento del robot. Este documento tambi�n incluye el esquema de conexiones.

El plan de administraci�n de configuraciones del proyecto se encuentra en el documento  Plan de administraci�n de configuraciones.pdf

Se sum� el documento Diagrama de componentes.pdf, que contiene el diagrama de componentes del proyecto.


- Siguiendo el esquema de directorio \sistemas_de_computacion\Codigo

El archivo hexapod.py contiene el c�digo principal del robot.

- Siguiendo el esquema de directorio \sistemas_de_computacion\Codigo\Modulo


En esta carpeta se encuentran los archivos: makefile, el archivo main_modulo_led es el archivo que llama a la funcionalidad del m�dulo, modulo_led contiene el c�digo del m�dulo, los otros archivos son resultado de la ejecuci�n del makefile: .modulo_led.ko, .modulo_led.mod.o, .modulo_led.o, main_modulo_led.so, Module.symvers, modules.order, modulo_led, modulo_led.ko (con este archivo instalamos el m�dulo),modulo_led.mod.o y modulo_led.o se encuentran en el directorio \sistemas_de_computacion\Codigo\Modulo\archivos_generados_al_ejecutar_el_makefile


- Siguiendo el esquema de directorio \sistemas_de_computacion\Codigo\asm_hexapodo

En esta carpeta se encuentran los archivos:
blink.s, contiene c�digo en asm, cdecl, Makefile y wrapper, contiene el llamado a la rutina en asm. Los archivos obtenidos al ejecutar el makefile se encuentran en el directorio \sistemas_de_computacion\Codigo\asm_hexapodo\archivos_generados_al_ejecutar_el_makefile



Respecto al m�dulo 


Un m�dulo es un fichero objeto que se puede "enlazar" y "des-enlazar" en el n�cleo de Linux en tiempo de ejecuci�n.�
Los m�dulos que el n�cleo puede cargar suelen residir en el directorio�/lib/modules/[uname -r]/.
Los m�dulos se crean (como cualquier fichero objeto) con el compilador de C de GNU:�gcc.�
Podemos insertar el m�dulo con la orden�insmod.
El kernel ofrece una serie de subrutinas o funciones en el espacio de usuario que permiten interaccionar con el hardware. Habitualmente, en sistemas UNIX o Linux, este di�logo se hace a trav�s de las funciones o subrutinas para leer y escribir en ficheros, ya que en estos sistemas los propios dispositivos se ven desde el punto de vista del usuario como ficheros. Por otro lado, en el espacio del kernel, Linux tambi�n ofrece una serie de funciones o subrutinas para por un lado interaccionar directamente, a bajo nivel, con los dispositivos hardware y por otro permite el paso de informaci�n desde el kernel al espacio de usuario.

En nuestro proyecto para llamar la funcionalidad simplemente se realiza una apertura del fichero en el momento en que se requiera.



