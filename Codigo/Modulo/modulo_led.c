/* Definiciones e includes necesarios para los drivers */
#include <linux/module.h>
#include <linux/kernel.h> /* printk() */
#include <linux/slab.h> /* kmalloc() */
#include <linux/fs.h> /* everything... */
#include <linux/uaccess.h> /* para copy_from_user */
#include <linux/device.h> 
#include <linux/kdev_t.h>
#include <linux/cdev.h>
#include <linux/printk.h>

#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>


MODULE_LICENSE("GPL");//licencia


int modulo_led_open(struct inode *inode, struct file *filp);
int modulo_led_release(struct inode *inode, struct file *filp);

void cleanup_module(void);


/* Definiciones de pines, direccion e inicilizacion de estado de GPIOs para LEDs */
static struct gpio leds[] = {
                { 20, GPIOF_OUT_INIT_LOW, "LED 2" },
                { 16, GPIOF_OUT_INIT_LOW, "LED 3" },
};




/* Estructura que declara las funciones tipicas de acceso a ficheros*/

struct file_operations modulo_led_fops = {

	open: modulo_led_open,
	release: modulo_led_release
};

/* Variables globales del driver */
/* Numero mayor */
int modulo_led_major; //numero que usa el kernel para ligar fichero con el driver
//cuando creamos el archivo de dipositivo los hacemos como # mknod /dev/modulo_led c 60 0


static struct class *classD; 
static struct device *modulo_ledDev;


int init_module(void) {

	int ret = 0;
	/*  Registrando dispositivo, 
		int register_chrdev (unsigned int major, const char *name, const struct file_operations *fops);  
		con major = 0, registra un numero mayor dinamicante y lo retorna y registra el dispositivo
	*/
	modulo_led_major = register_chrdev(0, "modulo_led", &modulo_led_fops);
	if (modulo_led_major < 0) { 
			  						 printk(KERN_INFO "Modulo_led: no se pudo registrar el dispositvo %d\n",modulo_led_major);
			 						 return modulo_led_major;
	}
	printk(KERN_INFO "Dispositivo registrado numero mayor:  %d\n",modulo_led_major);


	/*	Registrando clase del dispositivo
		struct class * class_create (struct module *owner, const char *name);	
		lo usamos para crear un puntero de clase struct que es usado para crera un dispositivo.
		se destruye con class destroy
	*/

	classD = class_create(THIS_MODULE, "Modulo_led");
	if (IS_ERR(classD)) {	
							//printk(KERN_INFO "Error registrando clase: %s\n",classD); *****************modificado
							unregister_chrdev(modulo_led_major, "modulo_led");
							return PTR_ERR(classD); 
	} 

	modulo_ledDev = device_create(classD, NULL, MKDEV(modulo_led_major, 0), NULL, "modulo_led");
	if(IS_ERR(modulo_ledDev)){
							printk(KERN_INFO "Error creando dispositivo \n");
							class_destroy (classD);
							unregister_chrdev(modulo_led_major, "modulo_led");
							return PTR_ERR(classD); 
	}	
	printk(KERN_INFO "Dispositivo creado: modulo_led \n");


        ret = gpio_request_array(leds, ARRAY_SIZE(leds));

        if (ret) {
                printk(KERN_ERR "request GPIOs desconocida: %d\n", ret);
                return ret;
        }


	printk(KERN_INFO "Modulo modulo_led: insertado y dispositivo creado \n");
	//return 0;
	return ret;

}


void cleanup_module(void) {
	
	int i;
	/* liberamos el dispositivo, clase,  numero major,  */	

	device_destroy(classD, MKDEV(modulo_led_major, 0) );
	class_destroy(classD);
	unregister_chrdev(modulo_led_major, "modulo_led");

        // turn all off
        for(i = 0; i < ARRAY_SIZE(leds); i++) {
                gpio_set_value(leds[i].gpio, 0);
        }

        // unregister
        gpio_free_array(leds, ARRAY_SIZE(leds));

	printk(KERN_INFO "Modulo modulo_led: quitado\n");

}

/* estructura inode pasa informacion del kernel al driver tal como el numero
   mayor y el numero menor. la estructura file con informacion relativa a las distintas
   operaciones que se pueden realizar con el fichero.  */
/* abrimos el dispositvo como fichero*/
int modulo_led_open(struct inode *inode, struct file *filp) {

	printk(KERN_INFO "Modulo modulo_led: modulo_led_open\n");


	    int j;

        //printk(KERN_INFO "%s\n", __func__);

        for(j = 0;j < 5;j++){
                        gpio_set_value(leds[0].gpio, 0);
                        gpio_set_value(leds[1].gpio, 1);
                        mdelay(500);
                        gpio_set_value(leds[0].gpio, 1);
                        gpio_set_value(leds[1].gpio, 0);
                        mdelay(500);
        }

        gpio_set_value(leds[0].gpio, 0);
        gpio_set_value(leds[1].gpio, 0);
	
	/* aca se deberia iniciar las variables que corresponde al driver y el dispositivo en si*/
	
	return 0;

}


/* cerramos el dispositivo como fichero, miembro release de la estructura file operations,
   mismos argumentos que la funcion anterior */
/* cerramos el dispositivo como fichero */
/* decrementamos la cuenta de uso para restrablecerla a su valor original. el modulo no se    	 
   podra descargar si la cuenta es distina de 0*/
int modulo_led_release(struct inode *inode, struct file *filp) {
	
printk(KERN_INFO "Modulo modulo_led: modulo_led_release\n");
/* aca se deberia liberar memoria y varibles relacionadas con la apertura del dispositivo*/
	

	return 0;
}




