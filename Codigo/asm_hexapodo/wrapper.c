#include "cdecl.h"
#include <stdio.h>

int PRE_CDECL main( void ) POST_CDECL;

int principal()
{
    printf("Iniciando wrapper assembler\n");
    main();
    printf("Finalizando wrapper\n");
    return 0;
}
